#!/bin/bash

WORKSPACE="$HOME/workspace"
NDK_URL="http://dl.google.com/android/ndk/android-ndk-r9b-linux-x86.tar.bz2"
UDEV_RULE="/etc/udev/rules.d/51-android.rules"
ADB_RULE="$HOME/.android/adb_usb.ini"

#For permision troubleshooting see: 
#http://stackoverflow.com/questions/14460656/android-debug-bridge-adb-device-no-permissions
#export PATH=${PATH}:/home/hetii/workspace/adt/sdk/platform-tools:/home/hetii/workspace/adt/sdk/tools/

install_packages(){
    sudo apt-get remove openjdk-6-jre
    sudo apt-get autoremove && apt-get clean
    sudo apt-get install software-properties-common
    sudo add-apt-repository ppa:webupd8team/java
    sudo apt-get update
    sudo apt-get install -y oracle-java6-installer
    sudo apt-get install -y build-essential kernel-package libncurses5-dev bzip2 android-tools-adb abootimg
    sudo apt-get install -y ia32-libs lib32z1-dev lib32ncurses5-dev gcc-multilib g++-multilib flex gperf lzop

    [ $? -gt 0 ] && echo "Error occured in install_packages" && exit
}

install_file(){
    if [ ! -f ${1} ];then
        echo "Install file `basename ${1}` to `dirname ${1}`"
        sudo cp `basename ${1}` `dirname ${1}`
    fi
    [ $? -gt 0 ] && echo "Error occured in install_file" && exit
}

download_file(){
    FN=`basename ${1}`
    if [ ! -f ${WORKSPACE}/${FN} ];then
        echo "Download file: ${FN}"
        wget -N -P ${WORKSPACE} ${1}
    else
        echo "File ${FN} already exist - No need to download."
    fi
    [ $? -gt 0 ] && echo "Error occured in download_file" && exit
}

extract_file()
{
     PWD=`pwd`
     FN=`basename ${1}`
     cd `dirname ${1}`
     if [ -f ${FN} ]; then
         case ${FN} in
             *.tar.bz2)   tar xvjf ${FN}   ;;
             *.tar.gz)    tar xvzf ${FN}   ;;
             *.bz2)       bunzip2 ${FN}    ;;
             *.rar)       unrar x ${FN}    ;;
             *.gz)        gunzip ${FN}     ;;
             *.tar)       tar xvf ${FN}    ;;
             *.tbz2)      tar xvjf ${FN}   ;;
             *.tgz)       tar xvzf ${FN}   ;;
             *.zip)       unzip ${FN}      ;;
             *.Z)         uncompress ${FN} ;;
             *.7z)        7z x ${FN}       ;;
             *)           echo "'${FN}' cannot be extracted via >extract<" ;;
         esac
     else
         echo "'${FN}' is not a valid file"
     fi
     cd ${PWD}
     [ $? -gt 0 ] && echo "Error occured in extract_file" && exit
}

#======================= IMPLEMENTATION =======================
prepare_env(){
    mkdir -p ${WORKSPACE}
}

prepare_ndk(){
    download_file ${NDK_URL}
    extract_file ${WORKSPACE}/`basename ${NDK_URL}`
    NDKBIN=$(dirname `find ${WORKSPACE} -name arm-linux-androideabi-gcc|tail -n 1`)
    echo "PATH=\$PATH:${NDKBIN}" >> $HOME/.bashrc
    echo "export CROSS_COMPILE=arm-linux-androideabi-" >> $HOME/.bashrc
    echo "export ARCH=arm" >> $HOME/.bashrc
}

prepare_udev(){
    #For other vendor please see: http://raw.github.com/apkudo/adbusbini/master/adb_usb.ini
    #More info at: http://blog.apkudo.com/2012/08/21/one-true-adb_usb-ini-to-rule-them-all 
    sudo rm -f `dirname ${UDEV_RULE}`/*android*
    install_file ${UDEV_RULE}

    #Generate adb_usb.ini based on our UDEV_RULE file.
    mkdir -p `dirname ${ADB_RULE}`
    echo "# ANDROID 3RD PARTY USB VENDOR ID LIST -- DO NOT EDIT." > ${ADB_RULE}
    echo "# USE 'android update adb' TO GENERATE." >> ${ADB_RULE}
    echo "# 1 USB VENDOR ID PER LINE." >> ${ADB_RULE}
    cat ${UDEV_RULE} | sed 's/.*=="\([0-9a-fA-F]\+\)".*/0x\1/'|sed -n '/#/!p' >> ${ADB_RULE}

    sudo chmod a+r ${UDEV_RULE}
    sudo service udev restart
    adb kill-server
    adb start-server
    adb devices
}

unblock_adb(){
    echo "#TODO implement it."
    #system/build.prop and added persist.service.adb.enable=1 at the bottom, I also added ro.setupwizard.mode=DISABLED
}

push_kernel(){
    if [ ! -f "boot.img" ];then
        adb shell dd if=/dev/block/mmcblk0p15 of=/cache/boot.img
	adb pull /cache/boot.img ./boot.img
    fi
    abootimg -i boot.img
    #cp ../linux-3.12-rc7/arch/arm/boot/zImage .
#    cp ../kernel/arch/arm/boot/zImage .
#    abootimg -i boot.img
#    abootimg -u boot.img -k zImage
#    abootimg -i boot.img
#    adb push boot.img /cache/
#    adb shell dd if=/cache/boot.img of=/dev/block/mmcblk0p15
#    adb shell reboot
}

get_cm_kernel(){
    git clone git://github.com/CyanogenMod/cm-kernel.git
}

get_props(){
    adb pull /system/build.prop
}

get_defconfig(){
    adb shell 'cat /proc/config.gz | gunzip' > ./running.config
}

readsms(){
    adb pull /data/data/com.android.providers.telephony/databases/mmssms.db .
    echo 'select address,"           ",body from sms;' |sqlite3 mmssms.db && rm -f mmssms.db
}

makecall(){
    adb shell am start -a android.intent.action.CALL -d tel:$1
}

cm_init(){
 #   sudo apt-get update
 #  sudo apt-get install ia32-libs lib32z1-dev lib32ncurses5-dev gcc-multilib g++-multilib flex
    mkdir $HOME/bin
    curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > $HOME/bin/repo
    chmod a+x $HOME/bin/repo
    echo "export PATH=\$PATH:\$HOME/bin" >> $HOME/.bashrc
    mkdir -p ~/android/system 
    cd ~/android/system 
    repo init -u git://github.com/CyanogenMod/android.git -b jellybean
}


#main(){
    #install_packages
    prepare_udev
    #prepare_env
    #prepare_ndk
    #readsms
    #push_kernel
#}
$1
